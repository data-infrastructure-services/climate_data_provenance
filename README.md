# Climate_data_provenance

- Combining data discovery and data use catalogs
- Use cases for PID collections
- W3C Prov for climate data provenance use cases 

This repo maintains illustrative examples of W3C based data provenance descriptions used in different climate data management and data processing use cases
and collects material on related aspects of data catalogs and PIDs 

Contents
in info folder: 
- Prov_intro.ipynb:  An intro into the used python provenance package
- PROV-Templates-python.ipynb: the Prov templating approach used 
- Data_ingest_use_case.ipynb: Data management use case using prov templating approach
- esmbal-prov.ipynb: Data processing use case using prov templating approach
- PID_examples.ipynb: example of retrieving PID related metadata based on the pyhandle library, PIDs are key references to reference to climate data in provenance records

catalogs folder:
- Using OAI harvestable catalog entries to combine discovery and use catalog information


in use_case_notebooks folder:
- prov examples for specific use cases ( .. to be updated ..)