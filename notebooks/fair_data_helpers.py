from provtemplates import provconv
import prov.model as prov
import requests
import json
import copy
import pyhandle

# Define a dictionary listing the namespaces used in the template 


def create_prov_template(): 

    ns_dict = {
        # required prov template namespaces
        'prov':'http://www.w3.org/ns/prov#',
        'var':'http://openprovenance.org/var#',
        'vargen':'http://openprovenance.org/vargen#',
        'tmpl':'http://openprovenance.org/tmpl#',
        # oftenly uses namespaces
        'foaf':'http://xmlns.com/foaf/0.1/',
        'rdf':'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'rdfs':'http://www.w3.org/2000/01/rdf-schema#',
        'xsd':'http://www.w3.org/2001/XMLSchema#',  
        'orcid':'http://orcid.org/',
        # example namespaces
        'ex': 'http://example.org/',
        'ex1': 'http://example.org/1/',
        'ex2': 'http://example.org/2/'
    }

    # Create an empyty prov document with all the namespaces
    prov_doc = provconv.set_namespaces(ns_dict,prov.ProvDocument())

    # Create a prov bundle with two entities
    bundle = prov_doc.bundle('vargen:bundleid')
    
    script = bundle.activity('var:nb_script')

    # ,(
    #           ('prov:label', 'ex:analysis_script'),
    #           ('rdf:URI', 'var:nb_github_ref'), 
    #          ))
        
    input_data = bundle.entity('var:input',(
            ('prov:PROV_TYPE', 'prov:COLLECTION'),
            ('prov:label', 'ex:PID_collection'),
            ('prov:value','var:in_coll_pid'),
        ))

    author = bundle.entity('var:author',(
            ('prov:PROV_TYPE', "prov:Person"),
            ('prov:label', 'ex:responsible_person'),
            ('foaf:name','var:resp_name'),
        ))
                        
    output_data = bundle.entity('var:output',(
            ('prov:PROV_TYPE', "prov:SOFTWAREAGENT"),
            ('prov:label', 'ex:PID_collection'),
            ('prov:value','var:out_coll_pid'),
            ))
        

    # Create relations between the two entities
    bundle.wasAttributedTo('var:nb_script','var:author')
    bundle.wasDerivedFrom('var:output','var:input')
    #bundle.wasAssociatedWith('var:author','var:nb_script')
    bundle.used('var:nb_script','var:input')
    bundle.wasGeneratedBy('var:output','var:nb_script')
    return prov_doc



def create_pid_collection(coll_pid,pid_list):
    headers = {'content-type':'application/json'}
    coll_url = "http://localhost:8080/api/v1/collections/"
    new_coll_url = coll_url+coll_pid
    print("new collection url",new_coll_url)
    
    coll_entry = {
        "id":coll_pid,
        "capabilities":{
            "isOrdered": True,
            "appendsToEnd": True,
            "supportsRoles": True,
            "membershipIsMutable": True,
            "propertiesAreMutable": True,
            "maxLength": -1
        },
        "properties":{
            "ownership":"DKRZ",
            "license": "CC-BY-NC-SA",
            "modelType": "21.T11148/2d1e64bc217fce96a569",
            "hasAccessRestrictions": False
        },
       "description": "PID collection"
       }
    
    coll_entry_list = [coll_entry]
    
    m_entry = {}
    m_entry["location"]="http://test"
    m_entry["description"]="CMIP6 data file PID"
    m_entry["datatype"]="21.T11148/ggguufallskdjfl9080"
    m_entry["mappings"]= {"role":"default","index":0}

    member_list = []
    index = 0
    for pid in pid_list:

        entry = copy.deepcopy(m_entry)
        entry["id"]=pid
        entry["mappings"]={"role":"default","index":index}
        member_list.append(entry)
        index+=1
        
    # create collection
    
    r = requests.post(coll_url, data=json.dumps(coll_entry_list), headers=headers)
    
    # add members
    
    mem_url = new_coll_url+"/members"
    print(mem_url)
    m = requests.post(mem_url, data=json.dumps(member_list), headers=headers)
    
    return (coll_pid, new_coll_url)

def register_handle(cred_path,unique_id,url,metadata_dict):
    # Dependency: https://github.com/EUDAT-B2SAFE/PYHANDLE
    # Test Prefix 21.T12996
   

    # Prepare the client that talks to the handle server (via http/REST):
    creds = pyhandle.clientcredentials.PIDClientCredentials.load_from_JSON(cred_path)
    client = pyhandle.handleclient.PyHandleClient('rest').instantiate_with_credentials(creds, HTTPS_verify=False)

    # Define the PID string:
    handle = unique_id
    # or with a uuid:
    #prefix = '21.14100'
    #handle = client.generate_PID_name(prefix)

    # The URL where the users will be redirected to:
    url = 'https://www.dkrz.de'

    # Other metadata fields:
    other_values = {
        'DESCRIPTION': 'test1 EOSC',
        'CONTACT': 'kindermann@dkrz.de',
        'CREATED': 20210625,
        'TEST':'TRUE'
    }

    # Actual creation:
    pid = client.register_handle(handle, url, overwrite=False, **metadata_dict)
    return pid